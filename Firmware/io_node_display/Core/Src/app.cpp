#include <cstdio>

#include "utils/dsb18b20.h"
#include "adc.h"
#include "temp_display.h"

void set_outputs(uint8_t outputs) {
}

void get_analogs(uint16_t *temperature, uint16_t *voltages) {
    *temperature = (uint16_t) ((float) get_temperature() * 1000.0f);

    float _voltages[8] = {0};
    adc_sample(_voltages);

    voltages[0] = (uint16_t)(DS18B20_sample(1) * 1000.0f);
    voltages[1] = (uint16_t)(_voltages[2] * 1000.0f);
    voltages[2] = (uint16_t)(_voltages[3] * 1000.0f);
    voltages[3] = (uint16_t)(_voltages[4] * 1000.0f);
}

static void make_output() {
//    GPIO_InitTypeDef GPIO_InitStruct = {0};
//    GPIO_InitStruct.Pin = IN_7_Pin;
//    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
//    GPIO_InitStruct.Pull = GPIO_NOPULL;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
//    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    // Streamlined configuration for PB07 as output OD
    // Set MODE7 = 0x03 (50 MHz Output) , CNF7 = 0x01 (open drain)
    GPIOB->CRL |= 0x03 << 28;
}

static void make_input() {
//    GPIO_InitTypeDef GPIO_InitStruct = {0};
//    GPIO_InitStruct.Pin = IN_7_Pin;
//    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//    GPIO_InitStruct.Pull = GPIO_NOPULL;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
//    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    //Streamlined configuration for PB07 as input
    // Set MODE7 = 0x00 (Input) , CNF7 = 0x01 (floating input)
    GPIOB->CRL &= ~(0x03 << 28);
}

extern "C" {
void app_init() {
    display_init();
    DS18B20_Init(IN_7_GPIO_Port, IN_7_Pin, make_input, make_output);
}

void app_run() {
    display_run();
}

}
