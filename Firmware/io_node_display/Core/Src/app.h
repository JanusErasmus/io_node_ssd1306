#pragma once
#ifdef __cplusplus
extern "C" {
#endif

void app_init();
void app_run();

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

namespace App {

}

#endif
