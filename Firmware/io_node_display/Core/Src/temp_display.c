/*
 * temp_display.c
 *
 *  Created on: Jan 17, 2021
 *      Author: jcera
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "adc.h"
#include "utils/ssd1306.h"
#include "utils/dsb18b20.h"
#include "value_counter.h"

#define RUN_PERIOD 100
#define SAMPLE_PERIOD 10000

static uint32_t sample_tick = 0;
static uint32_t run_tick = 0;

static value_counter_t ds_counter;
static value_counter_t lm2_counter;
static value_counter_t lm1_counter;

static int x_offset = 0;
char data[512];

void display_init(I2C_HandleTypeDef *handle)
{
    value_counter_init(&ds_counter, 25);
    value_counter_init(&lm1_counter, 25);
    value_counter_init(&lm2_counter, 25);

    ssd1306_Init(handle);
    ssd1306_SetCursor(0, 0);
    ssd1306_WriteString("SSD1306", Font_7x10, White);
    ssd1306_SetCursor(0, 12);
    ssd1306_WriteString("Besig...", Font_11x18, White);
    ssd1306_UpdateScreen();
}

//Run every 100 ms
void display_run()
{
    if(run_tick > HAL_GetTick())
        return;

    run_tick = HAL_GetTick() + RUN_PERIOD;


    int changed = 0;
    if(sample_tick < HAL_GetTick())
    {
        sample_tick = HAL_GetTick() + SAMPLE_PERIOD;

        changed = 1;
        if(x_offset++ > 65)
        {
            x_offset = 0;
        }

        //Sample LM sensors
        float voltages[8] = {0};
        adc_sample(voltages);
        float lm1_temp = (voltages[0] * 100.0f) - 273.0f;
        float lm2_temp = (voltages[1] * 100.0f) - 273.0f;

        //sample DS18B20
        float ds_temp = DS18B20_sample(1);

        value_counter_set(&ds_counter, ds_temp);
        value_counter_set(&lm1_counter, lm1_temp);
        value_counter_set(&lm2_counter, lm2_temp);
    }

    changed |= value_counter_tick(&ds_counter);
    changed |= value_counter_tick(&lm1_counter);
    changed |= value_counter_tick(&lm2_counter);


    if(changed)
    {
        ssd1306_Fill(Black);
        sprintf(data, "LM1: %0.1f", lm1_counter.value);
        ssd1306_SetCursor(x_offset, 0);
        ssd1306_WriteString(data, Font_7x10, White);
        sprintf(data, "     % .1f", lm1_counter.value - ds_counter.value);
        ssd1306_SetCursor(x_offset, 11);
        ssd1306_WriteString(data, Font_7x10, White);

        sprintf(data, "DS : %0.1f", ds_counter.value);
        ssd1306_SetCursor(x_offset, 27);
        ssd1306_WriteString(data, Font_7x10, White);

        sprintf(data, "     % .1f", lm2_counter.value - ds_counter.value);
        ssd1306_SetCursor(x_offset, 42);
        ssd1306_WriteString(data, Font_7x10, White);
        sprintf(data, "LM2: %0.1f", lm2_counter.value);
        ssd1306_SetCursor(x_offset, 53);
        ssd1306_WriteString(data, Font_7x10, White);
        ssd1306_UpdateScreen();
    }
}
