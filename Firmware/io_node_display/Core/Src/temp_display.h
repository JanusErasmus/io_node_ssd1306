/*
 * temp_display.h
 *
 *  Created on: Jan 17, 2021
 *      Author: jcera
 */

#ifndef SRC_TEMP_DISPLAY_H_
#define SRC_TEMP_DISPLAY_H_

#ifdef __cplusplus
extern "C" {
#endif

void display_init();
void display_run();     //Run every 100 ms


#ifdef __cplusplus
}
#endif
#endif /* SRC_TEMP_DISPLAY_H_ */
